import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMinus,
  faPlus,
  faTimes,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import "./../styles/cartInfo.css";

const CartItem = ({ product, onRemove, onDecrease, onIncrease }) => {
  return (
    <div className="cart-item">
      <div className="cart-item-content">
        <h3>{product.title}</h3>
        <p>Precio: ${Number(product.price).toFixed(2)}</p>
        <p>Cantidad: {product.quantity}</p>
      </div>
      <div className="cart-item-actions">
        <button className="action" onClick={() => onDecrease(product.id)}>
          <FontAwesomeIcon icon={faMinus} />
        </button>
        <button className="action" onClick={() => onIncrease(product.id)}>
          <FontAwesomeIcon icon={faPlus} />
        </button>
        <button className="action" onClick={() => onRemove(product.id)}>
          <FontAwesomeIcon icon={faTrash} />
        </button>
      </div>
    </div>
  );
};

const Cart = ({ cartItems, onRemove, onDecrease, onIncrease, onCloseCart }) => {
  const calculateTotalPrice = () => {
    return cartItems.reduce(
      (total, product) => total + product.price * product.quantity,
      0
    );
  };

  return (
    <div className="cart-info-container">
      <div className="cart-header">
        <h2 className="cart-title">Mi carrito</h2>
        <button className="close-cart-button" onClick={onCloseCart}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </div>
      {cartItems.length > 0 ? (
        <>
          {cartItems.map((product) => (
            <CartItem
              key={product.id}
              product={product}
              onRemove={onRemove}
              onDecrease={onDecrease}
              onIncrease={onIncrease}
            />
          ))}
          <p className="total-price">
            Precio Total: ${Number(calculateTotalPrice())?.toFixed(2)}
          </p>
        </>
      ) : (
        <p>No hay productos en el carrito.</p>
      )}
    </div>
  );
};

export default Cart;
