import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import "./../styles/productList.css";
import { COLORS } from "../constants/data";
import showNotification from "../utils/notifications";
import { addToCartAndUpdateStorage } from "../utils/Actions";

let colorIndex = 0;

const ProductCard = ({ product, setCartItems, setAllProducts }) => {
  const cardColor = COLORS[colorIndex];
  colorIndex = (colorIndex + 1) % COLORS.length;

  const handleDelete = async () => {
    const confirmResult = await Swal.fire({
      icon: "warning",
      title: "¿Estás seguro?",
      text: "Esta acción eliminará el producto permanentemente.",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Sí, eliminar",
      cancelButtonText: "Cancelar",
    });

    if (confirmResult.isConfirmed) {
      try {
        const existingProducts =
          JSON.parse(localStorage.getItem("products")) || [];
        const updatedProducts = existingProducts.filter(
          (p) => p.id !== product.id
        );
        localStorage.setItem("products", JSON.stringify(updatedProducts));
        setAllProducts(updatedProducts);

        showNotification(
          "success",
          "Éxito",
          "Producto eliminado correctamente"
        );
      } catch (error) {
        showNotification(
          "error",
          "Error",
          "Hubo un problema al eliminar el producto"
        );
      }
    }
  };

  const handleAddToCart = () => {
    addToCartAndUpdateStorage(product, setCartItems);
  };

  return (
    <div className="product-card" style={{ backgroundColor: cardColor }}>
      <FontAwesomeIcon
        icon={faTimes}
        className="delete-icon"
        onClick={handleDelete}
      />
      <div className="card-content">
        <h3 className="card-title">{product.title}</h3>
        <p className="card-details">{product.content.substring(0, 70)}...</p>
        <p>
          <strong>Precio:</strong> ${Number(product.price).toFixed(2)}
        </p>
        <p>
          <strong>Categoría:</strong> {product.category}
        </p>
        <div className="image-container">
          {product.imageUrl.map((url, index) => (
            <img
              key={index}
              src={url}
              alt={`Product ${index}`}
              className="product-image"
            />
          ))}
        </div>
        <p>
          <strong>Fecha de Publicación:</strong> {product.date}
        </p>
      </div>
      <div className="card-details">
        <Link to={`/product/${product.id}`} className="view-details-link">
          Ver Detalles
        </Link>
        <button className="add-to-cart-button" onClick={handleAddToCart}>
          Agregar al Carrito
        </button>
      </div>
    </div>
  );
};

const ProductList = ({ products, setCartItems, setAllProducts }) => {
  return (
    <div className="product-list-container">
      <h2 className="product-list-title">Listado de productos</h2>
      <div className="product-list">
        {products?.length > 0 ? (
          products.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
              setCartItems={setCartItems}
              setAllProducts={setAllProducts}
            />
          ))
        ) : (
          <div className="no-products-container">
            <p>No hay productos para mostrar.</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductList;
