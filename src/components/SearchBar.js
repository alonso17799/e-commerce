import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./../styles/searchBar.css";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SearchBar = ({ onSearch, isOnline, onShowCart }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [sortOrder, setSortOrder] = useState("asc");
  const navigate = useNavigate();

  const handleSearch = () => {
    onSearch(searchTerm);
  };

  const handleSort = () => {
    const newSortOrder = sortOrder === "asc" ? "desc" : "asc";
    setSortOrder(newSortOrder);
    onSearch(searchTerm, newSortOrder);
  };

  const handleGoToNewProduct = () => {
    navigate("/new-product");
  };

  return (
    <div className="search-bar-container">
      <input
        type="text"
        placeholder="Buscar productos..."
        className="search-input"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        disabled={!isOnline}
      />
      <button className="sort-button" onClick={handleSort} disabled={!isOnline}>
        Ordenar {sortOrder === "asc" ? "↑" : "↓"}
      </button>
      <button
        className="search-button"
        onClick={handleSearch}
        disabled={!isOnline}
      >
        Buscar
      </button>
      <button
        className="new-product-button"
        onClick={handleGoToNewProduct}
        disabled={!isOnline}
      >
        Nuevo Producto
      </button>
      <button
        className="view-cart-button"
        onClick={onShowCart}
        disabled={!isOnline}
      >
        <FontAwesomeIcon icon={faShoppingCart} />
      </button>
    </div>
  );
};

export default SearchBar;
