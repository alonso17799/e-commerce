import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "./../styles/productDetail.css";
import showNotification from "../utils/notifications";
import { addToCartAndUpdateStorage } from "../utils/Actions";

const ProductDetail = ({ setCartItems }) => {
  const { id } = useParams();
  const [product, setProduct] = useState(null);

  useEffect(() => {
    const fetchProduct = () => {
      try {
        const existingProducts =
          JSON.parse(localStorage.getItem("products")) || [];

        const foundProduct = existingProducts.find((item) => item.id === id);

        setProduct(foundProduct);
      } catch (error) {
        showNotification("error", "Error", "Error al cargar el producto");
      }
    };

    fetchProduct();
  }, [id]);

  if (!product) {
    return <p>Cargando...</p>;
  }

  const handleAddToCart = () => {
    addToCartAndUpdateStorage(product, setCartItems);
  };

  return (
    <div className="product-detail-container">
      <h2 className="product-detail-title">{product.title}</h2>
      <div className="product-detail-content">
        <p>{product.content}</p>
      </div>
      <p>
        <strong>Precio:</strong> ${Number(product.price).toFixed(2)}
      </p>
      <p>
        <strong>Categoría:</strong> {product.category}
      </p>
      <div className="image-container">
        {product.imageUrl.map((url, index) => (
          <img
            key={index}
            src={url}
            alt={`Product ${index}`}
            className="product-image"
          />
        ))}
      </div>
      <p className="product-detail-date">
        Fecha de Publicación: {product.date}
      </p>
      <button className="add-to-cart-button" onClick={handleAddToCart}>
        Agregar al Carrito
      </button>
    </div>
  );
};

export default ProductDetail;
