import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import "./../styles/productForm.css";
import showNotification from "../utils/notifications";
import { ERROR_MESSAGES } from "../constants/data";

const ProductForm = ({ isOnline, setAllProducts }) => {
  const [formData, setFormData] = useState({
    title: "",
    content: "",
    price: "",
    category: "",
    imageUrl: [],
  });
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setFormData((prevData) => ({
          ...prevData,
          imageUrl: [...prevData.imageUrl, reader.result],
        }));
      };
      reader.readAsDataURL(file);
    }
  };

  const saveProductToLocal = (product) => {
    const existingProducts = JSON.parse(localStorage.getItem("products")) || [];
    const updatedProducts = [...existingProducts, product];
    localStorage.setItem("products", JSON.stringify(updatedProducts));
    setAllProducts(updatedProducts);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (formData.imageUrl.length === 0) {
      showNotification("warning", "Advertencia", "Añade al menos una imagen");
      return;
    }

    try {
      const currentDate = moment().format("YYYY-MM-DD HH:mm:ss");
      const uniqueId = uuidv4();
      const product = {
        ...formData,
        date: currentDate,
        id: uniqueId,
      };
      saveProductToLocal(product);
      showNotification("success", "Éxito", "Producto guardado con éxito");
      navigate("/");
    } catch (error) {
      showNotification("error", "Error", ERROR_MESSAGES.saveProductError);
    }
  };

  return (
    <div className="product-form-container">
      <h2 className="product-form-title">Crear Producto</h2>
      <form className="product-form" onSubmit={handleSubmit}>
        <div>
          <label htmlFor="title" className="product-form-subtitle">
            Título
          </label>
          <input
            type="text"
            id="title"
            name="title"
            placeholder="Añade un título"
            value={formData.title}
            onChange={handleChange}
            className="product-form-input"
            required
          />
        </div>
        <div>
          <label htmlFor="price" className="product-form-subtitle">
            Precio
          </label>
          <input
            type="number"
            id="price"
            name="price"
            placeholder="Añade un precio"
            value={formData.price}
            onChange={handleChange}
            className="product-form-input"
            required
          />
        </div>
        <div>
          <label htmlFor="category" className="product-form-subtitle">
            Categoría
          </label>
          <input
            type="text"
            id="category"
            name="category"
            placeholder="Añade una categoría"
            value={formData.category}
            onChange={handleChange}
            className="product-form-input"
            required
          />
        </div>
        <div>
          <label htmlFor="content" className="product-form-subtitle">
            Descripción
          </label>
          <textarea
            id="content"
            name="content"
            value={formData.content}
            placeholder="Añade una descripción"
            onChange={handleChange}
            className="product-form-textarea"
            required
          />
        </div>
        <div className="container-images">
          <label htmlFor="imageUrl" className="product-form-subtitle">
            Seleccionar imagen
          </label>
          <input
            type="file"
            id="imageUrl"
            name="imageUrl"
            accept="image/*"
            onChange={handleImageChange}
            className="product-form-input"
          />
        </div>
        <div className="image-preview-container">
          {formData.imageUrl.map((url, index) => (
            <img
              key={index}
              src={url}
              alt={`Product ${index}`}
              className="image-preview"
            />
          ))}
        </div>
        <button
          type="submit"
          className="product-form-button"
          disabled={!isOnline}
        >
          Guardar Producto
        </button>
      </form>
    </div>
  );
};

export default ProductForm;
