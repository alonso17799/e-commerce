export const PRODUCTS = [
  {
    id: "1",
    title: "Producto 1",
    content: "Descripción del Producto 1",
    price: "29.99",
    category: "Electrónicos",
    imageUrl: [
      "https://www.sebandainsurance.com/es/blog/wp-content/uploads/2023/02/Portada-seguros-para-dispositivos-electronicos.jpg",
      "https://img.freepik.com/foto-gratis/arreglo-coleccion-estacionaria-moderna_23-2149309643.jpg?size=626&ext=jpg",
    ],
    date: "2024-02-28 10:30:00",
  },
  {
    id: "2",
    title: "Producto 2",
    content: "Descripción del Producto 2",
    price: "49.99",
    category: "Ropa",
    imageUrl: [
      "https://media.gq.com.mx/photos/63f53660937b5d59a717b034/16:9/w_1920,c_limit/ropa-barata-de-hombre-en-tendencia-febrero-2023.jpg",
    ],
    date: "2024-02-27 15:45:00",
  },
];

export const ERROR_MESSAGES = {
  saveProductError:
    "Hubo un problema al intentar guardar el producto. Por favor, inténtalo de nuevo.",
};

export const COLORS = ["#9da5b1", "#f9b115", "#e55353", "#2eb85c", "#3399ff"];
