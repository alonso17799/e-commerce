import Swal from "sweetalert2";

const showNotification = (icon, title, text) => {
  Swal.fire({
    icon: icon,
    title: title,
    text: text,
  });
};

export default showNotification;
