import showNotification from "./notifications";

export const addToCartAndUpdateStorage = (item, setCartItems) => {
  try {
    const existingCart = JSON.parse(localStorage.getItem("cart")) || [];
    const existingCartItem = existingCart.find(
      (cartItem) => cartItem.id === item.id
    );

    if (existingCartItem) {
      const updatedCart = existingCart.map((cartItem) =>
        cartItem.id === item.id
          ? { ...cartItem, quantity: cartItem.quantity + 1 }
          : cartItem
      );
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      setCartItems(updatedCart);
    } else {
      const updatedCart = [...existingCart, { ...item, quantity: 1 }];
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      setCartItems(updatedCart);
    }

    showNotification("success", "Éxito", "Producto agregado al carrito");
  } catch (error) {
    showNotification("error", "Error", "Error al agregar al carrito");
  }
};
