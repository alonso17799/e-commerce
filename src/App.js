import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ProductForm from "./components/ProductForm";
import ProductList from "./components/ProductList";
import SearchBar from "./components/SearchBar";
import ProductDetail from "./components/ProductDetail";
import CartInfo from "./components/CartInfo";
import { PRODUCTS as initialProducts } from "./constants/data";
import showNotification from "./utils/notifications";

const App = () => {
  const [cartItems, setCartItems] = useState([]);
  const [showCart, setShowCart] = useState(false);
  const [allProducts, setAllProducts] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const [isOnline, setIsOnline] = useState(navigator.onLine);

  useEffect(() => {
    fetchProducts();
    loadCart();
    window.addEventListener("online", handleOnlineStatusChange);
    window.addEventListener("offline", handleOnlineStatusChange);

    return () => {
      window.removeEventListener("online", handleOnlineStatusChange);
      window.removeEventListener("offline", handleOnlineStatusChange);
    };
  }, []);

  const handleShowCart = () => setShowCart(true);
  const handleCloseCart = () => setShowCart(false);

  const handleOnlineStatusChange = () => setIsOnline(navigator.onLine);

  const loadCart = () => {
    try {
      const storedCart = JSON.parse(localStorage.getItem("cart")) || [];
      setCartItems(storedCart);
    } catch (error) {
      showNotification(
        "error",
        "Error",
        "Error al cargar el carrito desde el localStorage"
      );
    }
  };

  const fetchProducts = () => {
    try {
      let existingProducts = JSON.parse(localStorage.getItem("products")) || [];
      if (existingProducts.length === 0) {
        existingProducts = initialProducts;
        localStorage.setItem("products", JSON.stringify(existingProducts));
      }

      setAllProducts(existingProducts);
      setSearchResults(existingProducts);
    } catch (error) {
      showNotification("error", "Error", "Error al obtener los productos:");
    }
  };

  const handleSearch = async (term, sortOrder) => {
    let results = allProducts.filter((product) =>
      product.title.toLowerCase().includes(term.toLowerCase())
    );
    if (sortOrder === "asc") {
      results.sort((a, b) => a.price - b.price);
    } else if (sortOrder === "desc") {
      results.sort((a, b) => b.price - a.price);
    }
    setSearchResults(results);
  };

  const updateCart = (updatedCartItems) => {
    setCartItems(updatedCartItems);
    localStorage.setItem("cart", JSON.stringify(updatedCartItems));
  };

  const addToCart = (productId) => {
    const productToAdd = allProducts.find(
      (product) => product.id === productId
    );

    if (productToAdd) {
      const existingCartItem = cartItems.find((item) => item.id === productId);

      if (existingCartItem) {
        const updatedCartItems = cartItems.map((item) =>
          item.id === productId
            ? { ...item, quantity: item.quantity + 1 }
            : item
        );
        updateCart(updatedCartItems);
      } else {
        const updatedCart = [...cartItems, { ...productToAdd, quantity: 1 }];
        updateCart(updatedCart);
      }
    }
  };

  const removeFromCart = (productId) => {
    const updatedCartItems = cartItems.filter((item) => item.id !== productId);
    updateCart(updatedCartItems);
  };

  const decreaseCartItemQuantity = (productId) => {
    const existingCartItem = cartItems.find((item) => item.id === productId);

    if (existingCartItem && existingCartItem.quantity > 1) {
      const updatedCartItems = cartItems.map((item) =>
        item.id === productId ? { ...item, quantity: item.quantity - 1 } : item
      );
      updateCart(updatedCartItems);
    } else {
      removeFromCart(productId);
    }
  };

  return (
    <Router>
      <header style={{ padding: "10px", marginLeft: "20px" }}>
        {!isOnline && <p>No hay conexión a internet</p>}
      </header>
      <div>
        <Routes>
          <Route
            path="/new-product"
            element={
              <ProductForm
                isOnline={isOnline}
                setAllProducts={setSearchResults}
              />
            }
          />
          <Route
            path="/"
            element={
              <>
                <SearchBar
                  onSearch={handleSearch}
                  isOnline={isOnline}
                  onShowCart={handleShowCart}
                />
                <ProductList
                  products={searchResults}
                  setCartItems={setCartItems}
                  setAllProducts={setSearchResults}
                />
              </>
            }
          />
          <Route
            path="/product/:id"
            element={<ProductDetail setCartItems={setCartItems} />}
          />
        </Routes>
      </div>
      {showCart && (
        <CartInfo
          cartItems={cartItems}
          onCloseCart={handleCloseCart}
          onRemove={removeFromCart}
          onDecrease={decreaseCartItemQuantity}
          onIncrease={addToCart}
        />
      )}
    </Router>
  );
};

export default App;
